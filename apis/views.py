from django.contrib.auth import authenticate, login, logout
from django.core.validators import validate_email, ValidationError
from django.shortcuts import render
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
from django.db import IntegrityError


class BaseView(View):
    @staticmethod
    def response(data={}, message='', status=200):
        result = {
            'data': data,
            'message': message,
        }
        return JsonResponse(result, status)


class UserCreateView(BaseView):
    # ajax 요청시 보안상문제로 csrf_exempt 를 사용하여 csrf 토큰을 사용하지 않게 한다.
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(UserCreateView, self).dispatch(request, *args, **kwargs)

    # if request.method == 'POST' 와 같은 작동
    def post(self, request):
        username = request.POST.get('username', '')
        if not username:
            return self.response(message='아이디를 입력하세요.', status=400)

        password = request.POST.get('password', '')
        if not password:
            return self.response(message='비밀번호를 입력하세요', status=400)

        email = request.POST.get('email', '')
        try:
            validate_email(email)
        except ValidationError:
            self.response(message='올바른 이메일을 입력하세요.', status=400)

        try:
            user = User.objects.create_user(username, email, password)
        except IntegrityError:
            return self.response(message='이미 존재하는 아이디입니다.', status=400)

        return self.response({'user.id': user.id})


class UserLoginView(BaseView):
    def post(self, request):
        username = request.POST.get('username', '')
        if not username:
            return self.response(message='아이디를 입력하세요.', status=400)

        password = request.POST.get('password', '')
        if not password:
            return self.response(message='비밀번호를 입력하세요', status=400)

        # authenticate 장고제공 username, password 등 잘못입력되면 none 반환
        user = authenticate(request, username=username, password=password)
        if user is None:
            return self.response(message='입력 정보를 확인하세요.', status=400)

        login(request, user)

        return self.response()


class UserLogoutView(BaseView):
    def get(self, request):
        logout(request)

        return self.response()